/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

/**
 *
 * @author Trung Nhan
 */
 
class ServerClientThread extends Thread {
  Socket serverClient;
  int clientNo;
  ServerClientThread(Socket inSocket,int counter){
    serverClient = inSocket;
    clientNo=counter;
  }
  public void run(){
    try{
      DataInputStream inStream = new DataInputStream(serverClient.getInputStream());
      DataOutputStream outStream = new DataOutputStream(serverClient.getOutputStream());
      String clientMessage="", serverMessage="";
//      while(!clientMessage.equals("bye")){
//        clientMessage=inStream.readUTF();
//        System.out.println("From Client-" +clientNo+ ": Number is :"+clientMessage);
//        serverMessage="From Server to Client-" + clientNo;
//        outStream.writeUTF(serverMessage);
//        outStream.flush();
//      }
      inStream.close();
      outStream.close();
      serverClient.close();
    }catch(Exception ex){
      System.out.println(ex);
    }finally{
      System.out.println("Client -" + clientNo + " exit!! ");
    }
  }
}